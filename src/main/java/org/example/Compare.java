package org.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Compare {
    public static void main(String[] args) {
        Map<String, Object> leftMap = convert("from_firestore.json");
        Map<String, Object> rightMap = convert("from_rowA.json");
        Map<String, Object> leftFlatMap = FlatMapUtil.flatten(leftMap);
        Map<String, Object> rightFlatMap = FlatMapUtil.flatten(rightMap);

        MapDifference<String, Object> difference = Maps.difference(leftFlatMap, rightFlatMap);

        System.out.println("Entries only in \"from_firestore\"\n--------------------------");
        difference.entriesOnlyOnLeft()
                .forEach((key, value) -> System.out.println(key + ": " + value));

        System.out.println("\n\nEntries only in \"from_rowA\"\n--------------------------");
        difference.entriesOnlyOnRight()
                .forEach((key, value) -> System.out.println(key + ": " + value));

        System.out.println("\n\nEntries differing\n--------------------------");
        difference.entriesDiffering()
                .forEach((key, value) -> System.out.println(key + ": " + value));
    }

    @SneakyThrows
    public static File getFile(String name) {
        return Paths.get("src", "main", "resources").resolve(name).toFile();
    }

    @SneakyThrows
    public static Map<String, Object> convert(String path) {
        TypeReference<HashMap<String, Object>> type = new TypeReference<>() {};
        return new ObjectMapper().readValue(getFile(path), type);
    }
}